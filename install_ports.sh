#!/bin/bash

sudo port selfupdate

sudo port -N install git
sudo port -N install fish
sudo port -N install emacs
sudo port -N install peco
sudo port -N install the_silver_searcher

# cmigemo で必要
sudo port -N install nkf

# GNU 製コマンドなど
sudo port -N install findutils
sudo port -N install coreutils

# プログラミング言語
sudo port -N install python36
sudo port -N install go
sudo port -N install opam
sudo port -N install ruby25

# 使うかもしれない
sudo port -N install tree
sudo port -N install wget
sudo port -N install tmux
sudo port -N install tig
sudo port -N install chromedriver
sudo port -N install ffmpeg
sudo port -N install fswatch
