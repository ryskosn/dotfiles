#!/bin/bash

go get -u -v golang.org/x/tools/...

go get -u -v github.com/golang/lint/golint
go get -u -v github.com/golang/dep/cmd/dep
go get -u -v github.com/dougm/goflymake
go get -u -v github.com/rogpeppe/godef
go get -u -v github.com/nsf/gocode

go get -u -v github.com/motemen/ghq
go get -u -v github.com/typester/gh-open
go get -u -v github.com/motemen/gore

go get -u -v golang.org/x/text/encoding/japanese
go get -u -v golang.org/x/text/transform
go get -u -v golang.org/x/oauth2

go get -u -v github.com/jlaffaye/ftp
go get -u -v github.com/timakin/gonvert
