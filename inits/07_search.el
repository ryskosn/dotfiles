;;;;; migemo
;; http://weblog.ymt2.net/blog/html/2013/08/23/install_migemo_to_emacs_24_3_1.html
;; http://rubikitch.com/2014/08/20/migemo/
(use-package migemo
  :ensure t
  :config
  (setq migemo-command "cmigemo")
  (setq migemo-options '("-q" "--emacs"))
  (setq migemo-dictionary "/usr/local/share/migemo/utf-8/migemo-dict")
  (setq migemo-user-dictionary nil)
  (setq migemo-regex-dictionary nil)
  (setq migemo-coding-system 'utf-8-unix)
  (load-library "migemo")
  (migemo-init)
  )

;; https://github.com/benma/visual-regexp.el
(use-package visual-regexp
  :ensure t
  :bind (("M-%" . vr/query-replace))
  )

;; http://d.hatena.ne.jp/IMAKADO/20080724/1216882563
;; https://kyanagimoto.blogspot.jp/2013/05/emacscolor-moccur.html
(use-package color-moccur
  :ensure t
  :bind (("M-o" . occur-by-moccur))
  :config
  ;; 複数の検索語や、特定のフェイスのみマッチ等の機能を有効にする
  ;; 詳細は http://www.bookshelf.jp/soft/meadow_50.html#SEC751
  (setq moccur-split-word t)
  ;; migemo が require できる環境なら migemo を使う
  ;; 第三引数が non-nil だと load できなかった場合にエラーではなく nil を返す
  (when (require 'migemo nil t)
    (setq moccur-use-migemo t))
  )

;; http://d.hatena.ne.jp/rubikitch/20130202/all
(use-package all-ext
  :ensure t
  )

;; http://codeout.hatenablog.com/entry/2014/07/08/185826
(use-package ag
  :ensure t)

;; http://emacs.rubikitch.com/wgrep/
(use-package wgrep
  :ensure t
  :config
  ;; e で wgrep モードにする
  (setq wgrep-enable-key "e")
  ;; wgrep 終了時にバッファを保存
  (setq wgrep-auto-save-buffer t)
  ;; read-only buffer にも変更を適用する
  (setq wgrep-change-readonly-file t)
  )

;; http://emacs.rubikitch.com/ripgrep/
(use-package ripgrep
  :ensure t
  :config
  (setq ripgrep-executable "/usr/local/bin/rg")
  (setq ripgrep-arguments '("-S"))
  )

;; http://emacs.rubikitch.com/org-seek/
(use-package org-seek
  :ensure t
  :commands (org-seek-string org-seek-regexp org-seek-headlines)
  :config
  (setq org-seek-search-tool 'ripgrep)
  )


;; https://qiita.com/icb54615/items/8cacf69fbcc7fc4f2ea5
;; (use-package moccur-edit
;;   :ensure t
;;   :config
;;   )
