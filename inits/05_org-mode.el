;; org-remember の設定
;; (require 'org)

(use-package org
  :ensure t
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture)
         ("C-c l" . org-store-link))
  :config
  ;; コードブロックを当該言語のモードでハイライトする
  (setq org-src-fontify-natively t)
  (setq org-use-fast-todo-selection t)
  ;; TODO キーワードの設定 "|" より右側は完了状態
  (setq org-todo-keywords
        '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCEL(c)" "PENDING(p)" )))
  ;; メモを格納する org ファイルの設定
  (setq org-directory "~/Dropbox/Org/")
  (setq org-capture-templates
        '(
          ("S" "SS" entry (file+headline "~/Dropbox/Org/ss.org" "Inbox")
           "* %U %?\n\n" :prepend t :empty-lines 1)

          ("d" "Diary" entry (file+headline "~/Dropbox/Org/diary.org" "Inbox")
           "* %U %?\n\n" :prepend t :empty-lines 1)

          ("n" "Note" entry (file "~/Dropbox/Org/note.org")
           "* %U %?\n\n" :prepend t :empty-lines 1)

          ("f" "Forex" entry (file+headline "~/Dropbox/Org/forex.org" "Inbox")
           "* %U %?\n\n" :prepend t :empty-lines 1)
          ))

  ;; http://d.hatena.ne.jp/tamura70/20100208/org
  ;; アジェンダ表示の対象ファイル
  (setq org-agenda-files (list org-directory))
  ;; アジェンダ表示で下線を用いる
  (add-hook 'org-agenda-mode-hook '(lambda () (hl-line-mode 1)))
  (setq hl-line-face 'underline)
  ;; org-mode 折り返しの設定
  ;; http://d.hatena.ne.jp/stakizawa/20091025/t1
  ;; M-x change-truncation で切り替え
  (setq org-startup-truncated nil)
  (defun change-truncation()
    (interactive)
    (cond ((eq truncate-lines nil)
           (setq truncate-lines t))
          (t
           (setq truncate-lines nil))))

  ;; http://rubikitch.com/2014/10/10/org-sparse-tree-indirect-buffer/
  (defun org-sparse-tree-indirect-buffer (arg)
    (interactive "P")
    (let ((ibuf (switch-to-buffer (org-get-indirect-buffer))))
      (condition-case _
          (org-sparse-tree arg)
        (quit (kill-buffer ibuf)))))
  (define-key org-mode-map (kbd "C-c /") 'org-sparse-tree-indirect-buffer)
  ;; org-refile
  (setq org-refile-targets
        (quote (
                ;; ("note.org" :level . 2)
                ("forex.org" :level . 1)
                ("trading-method.org" :level . 2)
                ("knowledge.org" :level . 1)
                )))

  ;; TODO 項目の追加 M-S-RET がなぜか効かないので
  ;; (global-set-key (kbd "C-c t") 'org-insert-todo-heading)
  ;; コードを評価するとき尋ねない
  (setq org-confirm-babel-evaluate nil)
)
