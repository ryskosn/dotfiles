;; Color theme
(load-theme 'wombat t)

;; スタートアップメッセージを表示しない
(setq inhibit-startup-screen t)

;; メニューバーを表示しない
(menu-bar-mode -1)

;; scratch buffer の初期メッセージを表示しない
(setq initial-scratch-message "")

;; 極力 utf-8 とする
(prefer-coding-system 'utf-8-unix)

;; 曜日表記を英語にする
(setq system-time-locale "C")

;; バックアップファイルを作らない
(setq make-backup-files nil)

;; オートセーブを無効にする
(setq auto-save-dafault nil)

;; yes or no を y or n にする
(fset 'yes-or-no-p 'y-or-n-p)

;; 補完時に大文字小文字を区別しない
(setq completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)

;; 補完候補を随時表示する
(icomplete-mode 1)

;; バッファ再読み込み
(global-auto-revert-mode 1)

;; 行番号の表示
(global-linum-mode)
(setq linum-format "%4d ")
;; https://qiita.com/takc923/items/acebbdae04994de16c6d
(setq linum-delay t)
(defadvice linum-schedule (around my-linum-schedule () activate)
  (run-with-idle-timer 0.2 nil #'linum-update-current))

;; paren-mode  対応する括弧を強調表示する
(show-paren-mode t)
(setq show-paren-delay 0)

;; スペルチェック
(setq-default flyspell-mode t)
(setq ispell-dictionary "american")

;; 最近使ったファイルの表示数
(setq recentf-max-menu-items 30)

;; 最近開いたファイルの保存数を増やす
(setq recentf-max-saved-items 3000)

;; ミニバッファの履歴を保存する
(savehist-mode t)

;; ミニバッファの履歴の保存数を増やす
(setq history-length 3000)

;; 行末の空白を強調表示
(setq-default show-trailing-whitespace t)
(set-face-background 'trailing-whitespace "#b14770")

;; trailing-whitespace 除外リスト
;; http://qiita.com/tadsan/items/df73c711f921708facdc
(defun my/disable-trailing-mode-hook ()
  "Disable show tail whitespace."
  (setq show-trailing-whitespace nil))

(defvar my/disable-trailing-modes
  '(comint-mode
    eshell-mode
    term-mode
    calendar-mode
    twittering-mode))
(mapc
 (lambda (mode)
   (add-hook (intern (concat (symbol-name mode) "-hook"))
             'my/disable-trailing-mode-hook))
 my/disable-trailing-modes)

;; 保存時に行末のスペースを削除
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; タブをスペースで扱う
(setq-default indent-tabs-mode nil)

;; タブ幅
(custom-set-variables '(tab-width 4))

;; dired を便利にする
(require 'dired-x)

;; dired から "r" でファイル名をインライン編集する
(require 'wdired)
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)

;; バッファ名を変更する
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; コントロール用のバッファを同一フレーム内に表示
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
;; diff のバッファを上下ではなく左右に並べる
(setq ediff-split-window-function 'split-window-horizontally)

;; 現在行のハイライト
(defface hlline-face
  '((((class color)
      (background dark))
     (:background "dark slate gray"))
    (((class color)
      (background light))
     (:background  "#98FB98"))
    (t
     ()))
  "*Face used by hl-line.")
(setq hl-line-face 'hlline-face)
(global-hl-line-mode)

;; キーストロークをエコーエリアに表示する時間を短く
(setq echo-keystrokes 0.1)

;;;;; server
(use-package server
  :config
  (unless (server-running-p)
    (server-start))
  )

;; common lisp
(require 'cl)

;; C-x C-u
(put 'upcase-region 'disabled nil)
;; C-x C-l
(put 'downcase-region 'disabled nil)
