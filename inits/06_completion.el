;; https://qiita.com/blue0513/items/acc962738c7f4da26656
;; http://glassonion.hatenablog.com/entry/2019/05/11/134135
(use-package eglot
  :ensure t
  :config
  (define-key eglot-mode-map (kbd "M-.") 'xref-find-definitions)
  (define-key eglot-mode-map (kbd "M-,") 'pop-tag-mark)
  (add-to-list 'eglot-server-programs '(go-mode . ("gopls")))
  (add-hook 'go-mode-hook 'eglot-ensure)
  )

;; ;; https://github.com/tigersoldier/company-lsp
;; (use-package company-lsp
;;   :ensure t
;;   :commands company-lsp
;; )

;; http://keisanbutsuriya.hateblo.jp/entry/2015/02/08/175005
(use-package auto-complete
  :ensure t
  :config
  (ac-config-default)
  (ac-set-trigger-key "TAB")
  ;; auto-complete までの時間
  (setq ac-delay 0.1)
  ;; メニューが表示されるまで
  (setq ac-auto-show-menu 0.1)
  ;; C-p, C-n で候補選択
  (setq ac-use-menu-map t)
  (setq ac-use-fuzzy t)
  ;; http://torotoki.hatenablog.com/entry/2013/06/05/032527
  (set-face-background 'ac-completion-face "#333333")
  (set-face-foreground 'ac-candidate-face "#666666")
  (set-face-background 'ac-selection-face "#666666")
  (set-face-foreground 'popup-summary-face "white")
  (set-face-background 'popup-tip-face "color-20")
  (set-face-foreground 'popup-tip-face "white")
  )

(use-package fuzzy
  :ensure t
  )

;; https://qiita.com/syohex/items/8d21d7422f14e9b53b17
;; (use-package company
;;   :ensure t
;;   :defer t
;;   :config
;;   (add-to-list 'company-backends 'company-lsp)
;;   (setq company-idle-delay 0)

;;   (global-set-key (kbd "C-M-i") 'company-complete)

;;   ;; C-n, C-p で補完候補を次/前の候補を選択
;;   (define-key company-active-map (kbd "C-n") 'company-select-next)
;;   (define-key company-active-map (kbd "C-p") 'company-select-previous)
;;   (define-key company-search-map (kbd "C-n") 'company-select-next)
;;   (define-key company-search-map (kbd "C-p") 'company-select-previous)
;;   ;; C-s で絞り込む
;;   (define-key company-active-map (kbd "C-s") 'company-filter-candidates)
;;   ;; TAB で候補を設定
;;   (define-key company-active-map (kbd "C-i") 'company-complete-selection)
;;   ;; 各種メジャーモードでも C-M-i で company-mode の補完を使う
;;   (define-key emacs-lisp-mode-map (kbd "C-M-i") 'company-complete)
;;   ;; C-h を無効化
;;   (define-key company-active-map (kbd "C-h") nil)
;;   ;; ドキュメント表示は C-S-h にする
;;   (define-key company-active-map (kbd "C-S-h") 'company-show-doc-buffer)

;;   ;; 見た目の設定
;;   (set-face-attribute 'company-tooltip nil
;;                       :foreground "black" :background "lightgrey")
;;   (set-face-attribute 'company-tooltip-common nil
;;                       :foreground "black" :background "lightgrey")
;;   (set-face-attribute 'company-tooltip-common-selection nil
;;                       :foreground "white" :background "steelblue")
;;   (set-face-attribute 'company-tooltip-selection nil
;;                       :foreground "black" :background "steelblue")
;;   (set-face-attribute 'company-preview-common nil
;;                       :background nil :foreground "lightgrey" :underline t)
;;   (set-face-attribute 'company-scrollbar-fg nil
;;                       :background "orange")
;;   (set-face-attribute 'company-scrollbar-bg nil
;;                       :background "gray40")
;;   )

;; https://github.com/joaotavora/yasnippet
;; http://konbu13.hatenablog.com/entry/2014/01/12/113300
;; https://www-he.scphys.kyoto-u.ac.jp/member/shotakaha/dokuwiki/doku.php?id=toolbox:emacs:yasnippet:start
(use-package yasnippet
  :ensure t
  :bind (:map yas-minor-mode-map
         ("C-x i i" . yas-insert-snippet)
         ("C-x i n" . yas-new-snippet)
         ("C-x i v" . yas-visit-snippet-file)
         ("C-x i l" . yas-describe-tables)
         ("C-x i g" . yas-reload-all)
         )
  :config
  (yas-global-mode 1)
  )

;; https://github.com/AndreaCrotti/yasnippet-snippets
(use-package yasnippet-snippets
  :ensure t
)
　
