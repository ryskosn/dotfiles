;; C-h を backspace にする
(global-set-key (kbd "C-h") 'delete-backward-char)

;; C-x o (other-window) の代わり
;; (global-set-key (kbd "C-t") 'other-window)
;; http://d.hatena.ne.jp/rubikitch/20100210/emacs
(defun other-window-or-split ()
  (interactive)
  (when (one-window-p)
    (split-window-horizontally))
  (other-window 1))
(global-set-key (kbd "C-t") 'other-window-or-split)
