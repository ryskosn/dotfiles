;; http://blog.shibayu36.org/entry/2017/04/02/193000
(use-package python-mode
  :defer t
  :config
  (add-hook 'python-mode-hook #'flycheck-mode)
  (setq auto-mode-alist (cons '("\\.py\\'" . python-mode) auto-mode-alist))
  )

;; http://d.hatena.ne.jp/n-channel/20131220/1387551080
(use-package epc
  :ensure t
  :defer t
  )

;; https://github.com/tkf/emacs-jedi
(use-package jedi
  :ensure t
  :defer t
  :config
  (add-hook 'python-mode-hook 'jedi:setup)
  (setq jedi:complete-on-dot t)
  )

;; https://github.com/paetzke/py-yapf.el
(use-package py-yapf
  :ensure t
  :defer t
  :config
  (add-hook 'python-mode-hook 'py-yapf-enable-on-save)
  )
