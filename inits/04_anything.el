(use-package anything
  :ensure t
  :bind (("C-o" . anything)
         ("C-x C-b" . anything-filelist+)
         ("M-y" . anything-show-kill-ring))
  :config
  (setq anything-sources
      '(anything-c-source-buffers+
        anything-c-source-recentf
        ;; anything-c-source-filelist
        anything-c-source-files-in-current-dir
        ;; anything-c-source-locate
        anything-c-source-file-name-history
        anything-c-source-man-pages
        anything-c-source-emacs-commands
        anything-c-source-emacs-functions
	))
  (setq anything-enable-shortcuts 'alphabet)
  (setq anything-quick-update t)
  )

(use-package anything-startup)
(use-package anything-config)
(use-package anything-match-plugin)
(use-package anything-complete)
(use-package anything-show-completion)
(anything-read-string-mode +1)
