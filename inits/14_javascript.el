;; https://emacs.cafe/emacs/javascript/setup/2017/04/23/emacs-setup-javascript.html
(use-package js2-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
  (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
  )

(use-package js2-refactor
  :ensure t
  :bind (:map js2-mode-map
              ("C-k" . #'js2r-kill))
  :config
  (add-hook 'js2-mode-hook #'js2-refactor-mode)
  (js2r-add-keybindings-with-prefix "C-c C-r")
  )

(use-package xref-js2
  :ensure t
  :bind (:map js-mode-map
              ("M-." . nil))
  :config
  (add-hook 'js2-mode-hook (lambda ()
  (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))
  )

;; https://qiita.com/ybiquitous/items/22ca5b8335fdf71967e8
;; need to install eslint
(use-package js-auto-format-mode
  :ensure t
  :config
  (add-hook 'js-mode-hook #'js-auto-format-mode)
  )

(use-package add-node-modules-path
  :ensure t
  :config
  (add-hook 'js-mode-hook #'add-node-modules-path)
  )

;; https://github.com/prettier/prettier-emacs
