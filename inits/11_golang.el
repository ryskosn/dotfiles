;; go-mode
;; global-whitespace-mode を使うときは indent-tabs-mode を nil にすること、company が誤作動する
;; 事前に go get golang.org/x/tools/cmd/goimports しておくこと
;; http://glassonion.hatenablog.com/entry/2019/05/11/134135
;; (let ((envs '("GOROOT" "GOPATH")))
;;   (exec-path-from-shell-copy-envs envs))

(use-package go-mode
  :commands go-mode
  :ensure t
  :defer t
  :init
  (add-hook 'go-mode-hook 'eglot-ensure)
  :config
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save)
)


;; (use-package go-mode
;;   :commands go-mode
;;   :ensure t
;;   :bind (:map go-mode-map
;;               ("M-." . godef-jump)
;;               ("M-," . pop-tag-mark))
;;   :init
;;   (add-hook 'go-mode-hook 'eglot-ensure)
;;   ;; (add-hook 'go-mode-hook 'company-mode)
;;   :config
;;   (add-hook 'before-save-hook 'gofmt-before-save)
;;   ;; (add-hook 'before-save-hook 'lsp-format-buffer)
;;   ;; gofmt を goimports に上書き
;;   (setq gofmt-command "goimports")
;;   )

;; (use-package go-autocomplete
;;   :ensure t
;;   )
