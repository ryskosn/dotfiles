;;;;; popwin
;; http://valvallow.blogspot.jp/2011/03/emacs-popwinel.html
;; https://github.com/m2ym/popwin-el
(use-package popwin
  :ensure t)

(setq display-buffer-function 'popwin:display-buffer)
(setq popwin:special-display-config
      (append '(
                ("*Remember*" :stick t)
                ("*Org Agenda*" :height 0.5)
                ("anything" :regexp t :height 0.5)
                ("*quickrun*" :height 0.4)
                ("magit" :regexp t :height 0.5)
                ("*Dired*" :height 0.5)
                ("*sdic*" :noselect)
                ("*Backtrace*")
                ("COMMIT_EDITMSG")
                )
              popwin:special-display-config))
(defvar popwin:special-display-config-backup popwin:special-display-config)
(global-set-key (kbd "C-x p") 'popwin:display-last-buffer)
;; (define-key dired-mode-map "o" #'(lambda ()
;;                                    (interactive)
;;                                    (popwin:find-file (dired-get-file-for-visit))))



;;;;; 分割したバッファを入れ替える
;; http://www.bookshelf.jp/soft/meadow_30.html#SEC403
(defun swap-screen()
  "Swap two screen,leaving cursor at current window."
  (interactive)
  (let ((thiswin (selected-window))
        (nextbuf (window-buffer (next-window))))
    (set-window-buffer (next-window) (window-buffer))
    (set-window-buffer thiswin nextbuf)))
(defun swap-screen-with-cursor()
  "Swap two screen,with cursor in same buffer."
  (interactive)
  (let ((thiswin (selected-window))
        (thisbuf (window-buffer)))
    (other-window 1)
    (set-window-buffer thiswin (window-buffer))
    (set-window-buffer (selected-window) thisbuf)))
;; F2 で入れ替え
(global-set-key [f2] 'swap-screen)
;; shift + F2 でカーソルごと入れ替え
(global-set-key [S-f2] 'swap-screen-with-cursor)


;;;;; window resize
;; http://d.hatena.ne.jp/khiker/20100119/window_resize
;; http://d.hatena.ne.jp/mooz/20100119/p1
(defun my-window-resizer ()
  "Control window size and position."
  (interactive)
  (let ((window-obj (selected-window))
        (current-width (window-width))
        (current-height (window-height))
        (dx (if (= (nth 0 (window-edges)) 0) 1
              -1))
        (dy (if (= (nth 1 (window-edges)) 0) 1
              -1))
        action c)
    (catch 'end-flag
      (while t
        (setq action
              (read-key-sequence-vector (format "size[%dx%d]"
                                                (window-width)
                                                (window-height))))
        (setq c (aref action 0))
        (cond ((= c ?l)
               (enlarge-window-horizontally dx))
              ((= c ?h)
               (shrink-window-horizontally dx))
              ((= c ?j)
               (enlarge-window dy))
              ((= c ?k)
               (shrink-window dy))
              ;; otherwise
              (t
               (let ((last-command-char (aref action 0))
                     (command (key-binding action)))
                 (when command
                   (call-interactively command)))
               (message "Quit")
               (throw 'end-flag t)))))))
(global-set-key (kbd "C-c C-r") 'my-window-resizer)


;; http://d.hatena.ne.jp/rubikitch/20080923/1222104034
(use-package open-junk-file
  :ensure t
  :bind (("C-x j" . open-junk-file))
  :config
  (setq open-junk-file-format "~/Dropbox/junk/%Y%m%d-%H%M%S." )
  )

;; http://d.hatena.ne.jp/uk-ar/20120401/1333282805
(use-package flex-autopair
  :ensure t
  :config
  (flex-autopair-mode +1)
  )

;; http://emacs.rubikitch.com/pangu-spacing/
(use-package pangu-spacing
  :ensure t
  :config
  ;; chinse-two-byte → japanese に置き換えるだけで日本語でも使える
  (setq pangu-spacing-chinese-before-english-regexp
        (rx (group-n 1 (category japanese))
            (group-n 2 (in "a-zA-Z0-9"))))
  (setq pangu-spacing-chinese-after-english-regexp
        (rx (group-n 1 (in "a-zA-Z0-9"))
            (group-n 2 (category japanese))))
  ;; 見た目ではなくて実際にスペースを入れる
  (setq pangu-spacing-real-insert-separtor t)
  ;; text-mode やその派生モード(org-mode 等)のみに使いたいならこれ
  ;; (add-hook 'text-mode-hook 'pangu-spacing-mode)
  ;; すべてのメジャーモードに使ってみたい人はこれを
  (global-pangu-spacing-mode +1)
  )

;; https://github.com/syohex/emacs-quickrun
;; http://yunojy.github.io/blog/2013/03/17/emacs-de-quickrun-or-quickrun-region/
(use-package quickrun
  :ensure t
  :bind (("M-q" . quickrun-sc))
  :config
  (defun quickrun-sc (start end)
    (interactive "r")
    (if mark-active
        (quickrun :start start :end end)
      (quickrun)))
  )

;; https://qiita.com/senda-akiha/items/cddb02cfdbc0c8c7bc2b
;; https://qiita.com/noriaki/items/8122e83867ff0cdb5d13
(use-package flycheck
  :ensure t
  :bind (("C-c C-n" . flycheck-next-error)
         ("C-c C-p" . flycheck-previous-error))
  :config
  (add-hook 'sql-mode-hook 'flycheck-mode)
  )

;; http://moonstruckdrops.github.io/blog/2013/03/24/markdown-mode/
(use-package markdown-mode
  :ensure t
  :mode ("\\.md\\'" . markdown-mode)
  )

;; https://github.com/magit/magit
;; https://magit.vc/
;; https://qiita.com/maueki/items/70dbf62d8bd2ee348274
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status))
  )
