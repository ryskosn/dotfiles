;; http://blog.515hikaru.net/entry/2016/09/12/021206
;; http://futurismo.biz/archives/2662

(use-package haskell-mode
  :ensure t
  :config
  (autoload 'haskell-mode "haskell-mode" nil t)
  (autoload 'haskell-cabal "haskell-cabal" nil t)

  (add-to-list 'auto-mode-alist '("\\.hs$" . haskell-mode))
  (add-to-list 'auto-mode-alist '("\\.lhs$" . literate-haskell-mode))
  (add-to-list 'auto-mode-alist '("\\.cabal$" . haskell-cabal-mode))

  (autoload 'ghc-init "ghc" nil t)
  (autoload 'ghc-debug "ghc" nil t)

  ;; フックを設定
  (add-hook 'haskell-mode-hook 'haskell-indentation-mode)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
  (add-hook 'haskell-mode-hook 'haskell-decl-scan-mode)
  (add-hook 'haskell-mode-hook 'haskell-doc-mode)
  ;; (add-hook 'haskell-mode-hook 'view-mode)
  (add-hook 'haskell-mode-hook #'flycheck-mode)

  (setq haskell-process-type 'stack-ghci)
  (setq haskell-process-path-ghci "stack")
  (setq haskell-process-args-ghci "ghci")
  )
;; (defun my-haskell-mode-hook ()
;;     (interactive)
;;     ;; インデント
;;     (turn-on-haskell-indentation)
;;     (turn-on-haskell-doc-mode)
;;     (font-lock-mode)
;;     (imenu-add-menubar-index)
;;     ;; GHCi のコマンドを設定
;;     (setq haskell-program-name "/usr/lobal/bin/stack ghci") ;; stack の場合
;;     (inf-haskell-mode)
;;     ;; ghc-mod を使えるように
;;     (ghc-init)
;;     ;; flycheck を起動
;;     (flycheck-mode))
;; (add-hook 'haskell-mode-hook 'my-haskell-mode-hook)
