(use-package elm-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.elm\\'" . elm-mode))
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-elm))
  (setq elm-format-on-save t)
  )
