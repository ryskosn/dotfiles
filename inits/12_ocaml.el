;; https://github.com/realworldocaml/book/wiki/Installation-Instructions

(use-package tuareg
  :ensure t
  :config
  (add-hook 'tuareg-mode-hook #'flycheck-mode)
  (add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)
  (setq auto-mode-alist
        (append '(("\\.ml[ily]?$" . tuareg-mode)
                  ("\\.topml$" . tuareg-mode))
                auto-mode-alist))
  (setq tuareg-use-smie nil)
  )

;; (autoload 'utop-setup-ocaml-buffer "utop" "Toplevel for OCaml" t)
;; Automatically load utop.el
(autoload 'utop "utop" "Toplevel for OCaml" t)
(autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
(add-hook 'tuareg-mode-hook 'utop-minor-mode)
;; Use the opam installed utop
(setq utop-command "opam config exec -- utop -emacs")

;; from merlin install message
(let ((opam-share (ignore-errors (car (process-lines "opam" "config" "var" "share")))))
      (when (and opam-share (file-directory-p opam-share))
       ;; set load-path
       (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))
       ;; Register Merlin
       (autoload 'merlin-mode "merlin" nil t nil)
       ;; Automatically start it in OCaml buffers
       (add-hook 'tuareg-mode-hook 'merlin-mode t)
       (add-hook 'caml-mode-hook 'merlin-mode t)
       ;; Use opam switch to lookup ocamlmerlin binary
       (setq merlin-command 'opam)))

(use-package merlin
  :ensure t
  :config
  ;; Enable auto-complete
  (setq merlin-use-auto-complete-mode 'easy)
  (setq merlin-error-after-save nil)
  )

(use-package ocp-indent
  :ensure t)

;; https://github.com/ocaml-ppx/ocamlformat
(require 'ocamlformat)
(add-hook 'tuareg-mode-hook (lambda ()
  (define-key tuareg-mode-map (kbd "C-M-<tab>") #'ocamlformat)
  (add-hook 'before-save-hook #'ocamlformat-before-save)))

;; https://github.com/Khady/merlin-eldoc
(use-package merlin-eldoc
  :after merlin
  :ensure t
  :custom
  (eldoc-echo-area-use-multiline-p t)
  (merlin-eldoc-max-lines 6)
  :hook (( tuareg-mode reason-mode) . merlin-eldoc-setup)
)
