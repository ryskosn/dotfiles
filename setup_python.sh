#!/bin/bash

# after install python3.7 by MacPorts
python3.7 -m venv ~/py37

# ~/py37 に PATH が通った状態で
pip install -U pip

pip install -r ~/dotfiles/requirements.txt
