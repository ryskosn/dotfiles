#!/bin/bash

opam install -y utop
opam install -y ppx_inline_test
opam install -y spotlib
opam install -y merlin
opam install -y ocamlspot
opam install -y ocamlformat
opam install -y core
opam install -y ocp-indent
opam install -y js_of_ocaml
