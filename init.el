(require 'package)
(package-initialize)
(setq package-archives
      '(("gnu"."http://elpa.gnu.org/packages/")
        ("melpa"."http://melpa.org/packages/")
        ("melpa-stable"."https://stable.melpa.org/packages/")
        ("org"."http://orgmode.org/elpa/")))

(unless package-archive-contents
  (package-refresh-contents))

;;;;; set load-path
;; (setq load-path
;;       (append
;;        (list
;;         (expand-file-name "~/.emacs.d/")
;;         (expand-file-name "~/.emacs.d/elisp")
;;         )
;;        load-path))

;;;;; ensure to use use-package
(when (not (package-installed-p 'use-package))
  (package-install 'use-package))
(require 'use-package)

;;;;; init-loader
(use-package init-loader
  :ensure t
  :init
  (setq init-loader-show-log-after-init nil)
  :config
  (init-loader-load "~/.emacs.d/inits")
  )

;;;;; custom-file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))
