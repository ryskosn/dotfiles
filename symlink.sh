#!/bin/bash

ln -s ~/dotfiles/.gitconfig ~/.gitconfig
ln -s ~/dotfiles/.gitignore ~/.gitignore

ln -s ~/dotfiles/config.fish ~/.config/fish/config.fish
ln -s ~/dotfiles/.slate.js ~/.slate.js

ln -s ~/dotfiles/init.el ~/.emacs.d/init.el
ln -s ~/dotfiles/inits ~/.emacs.d/inits
ln -s ~/dotfiles/custom.el ~/.emacs.d/custom.el

# ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf

echo set symbolic link done!
