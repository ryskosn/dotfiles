# PATH
set -x PATH /opt/local/bin /opt/local/sbin $PATH
set -x PATH /usr/local/bin $PATH

## Python
set -x PATH $HOME/py37/bin $PATH
# set -x PATH $HOME/py27/bin $PATH
alias python2 "$HOME/py27/bin/python2"
alias pip2 "$HOME/py27/bin/pip2"

## Go
set -x GOPATH $HOME
set -x PATH $GOPATH/bin $GOROOT/bin $PATH

## nodebrew (node.js)
set -x PATH $HOME/.nodebrew/current/bin $PATH

## stack (Haskell)
# set -x PATH $HOME/.local/bin $PATH

## Heroku Toolbelt
# set -x PATH /usr/local/heroku/bin $PATH

## OCaml
eval (opam config env)

## .NET
set -x PATH $HOME/.dotnet/tools $PATH

# alias
alias ls "ls -aG"
alias rm "rm -i"
alias cp "cp -i"
alias mv "mv -i"
alias mkdir "mkdir -p"

alias find "gfind"
alias xargs "gxargs"
alias ec "emacsclient -nw"

## alias for git
alias gst "git status"
alias gdif "git diff"
alias gdifc "git diff --cached"

## alias for macOS quicklook
alias ql "qlmanage -p $argv[1]"


# user defined functions
function cd
  builtin cd $argv
  ls
end

## peco with option
function peco
  command peco --layout=bottom-up $argv
end

function peco_select_history
    if test (count $argv) = 0
        set peco_flags --layout=bottom-up
    else
        set peco_flags --layout=bottom-up --query "$argv"
    end
    history|peco $peco_flags|read foo
    if [ $foo ]
        commandline $foo
    else
        commandline ''
    end
end


## ghq

# https://github.com/yoshiori/fish-peco_select_ghq_repository/blob/master/functions/peco_select_ghq_repository.fish
function peco_select_ghq_repository
  set -l query (commandline)

  if test -n $query
    set peco_flags --query "$query"
  end

  ghq list | peco $peco_flags | read line

  if [ $line ]
    ghq root | read dir
    cd $dir/$line
    commandline -f repaint
  end
end


function _peco_change_directory
  if [ (count $argv) ]
    peco --query "$argv "| perl -pe 's/([ ()])/\\\\$1/g'| read foo
  else
    peco perl -pe 's/([ ()])/\\\\$1/g'| read foo
  end
  if [ $foo ]
    builtin cd $foo
  else
    commandline ''
  end
end
function peco_change_directory
  begin
    echo $HOME/.config
    ls -ad */| perl -pe "s#^#$PWD/#" | egrep -v "^$PWD/\." | head -n 5
    sort -r -t '|' -k 3 ~/.z | sed -e 's/\|.*//'
    ghq list -p
    ls -ad */| perl -pe "s#^#$PWD/#" | grep -v \.git
  end | sed -e 's/\/$//' | awk '!a[$0]++' | _peco_change_directory $argv
end


# key bindings
function fish_user_key_bindings
  # C-r でコマンド履歴を peco 検索
  bind \cr peco_select_history
  # C-o で ghq で入れたリポジトリを検索して cd
  bind \co peco_change_directory
end