## macOS

```sh
# Xcode command line tools をインストールする
$ xcode-select --install

# dotfiles リポジトリを clone してくる
$ git clone git@bitbucket.org:ryskosn/dotfiles.git
$ cd dotfiles

# macOS の各種設定
$ bash defaults_config.sh
```

## Dropbox

https://www.dropbox.com/ja/downloading

### Chrome

https://www.google.co.jp/chrome/browser/desktop/index.html

### KeePassXC

https://keepassxc.org/download

### Karabiner-Elements

https://pqrs.org/osx/karabiner/

- Command キーを単体で IME 切り替えにする
- Emacs キーバインドにする
- `C-i` -> `TAB` を無効化した JSON を `~/.config/karabiner/assets/complex_modifications` に置く

see also

- [Karabiner-Elements の設定項目をまとめました - Qiita](https://qiita.com/s-show/items/a1fd228b04801477729c)


### Google IME

https://www.google.co.jp/ime/

システム環境設定 -> キーボード

- ひらがな
- U.S.

の二択で OK

#### 一般

- スペースの入力 -> 半角にする
- キー設定の選択 -> カスタム、インポートする
- Dropbox から `GoogleIME_keymap.txt` をインポートする

#### Advanced

- アルファベット -> 半角
- 数字 -> 半角
- サジェスト件数 -> 5


### Quicksilver

https://qsapp.com/download.php

- `Shortcut` を `Command + Space` にする
- `When activated, switch keyboard to` を `U.S.` にする
- `Reset search after` のチェックをオフにする

[![https://gyazo.com/a4486a72ffad2850f4edf93d3bdfce5f](https://i.gyazo.com/a4486a72ffad2850f4edf93d3bdfce5f.png)](https://gyazo.com/a4486a72ffad2850f4edf93d3bdfce5f)


### iTerm2

https://iterm2.com/downloads.html


#### Profiles - Text

- Cursor: Box, Blinking
- Font: `18pt` `Monaco`
- Non-ASCII Font: `18pt` `Osaka`

#### Profiles - Terminal

- Unlimited scrollback

#### Profiles -Keys

- Left option key act as: `Normal`
- Right option key act as: `Esc+`

#### Keys - Remap Modifier Keys

- Left command key: `Right Option`

##### see also

http://ryskosn.hatenadiary.com/entry/20141011/1413036752

#### Keys - Key Mappings

- (Left) `Command` `Tab`: Do Not Remap Modifiers

[![https://gyazo.com/15d74c7bc4d50f28680bcdc9771dfc8e](https://i.gyazo.com/15d74c7bc4d50f28680bcdc9771dfc8e.png)](https://gyazo.com/15d74c7bc4d50f28680bcdc9771dfc8e)


## MacPorts

以下よりインストーラーをダウンロードする

https://www.macports.org/install.php

`install_ports.sh` を実行

```sh
#!/bin/bash

sudo port selfupdate

sudo port -N install git
sudo port -N install fish
sudo port -N install emacs
sudo port -N install peco
sudo port -N install the_silver_searcher

# cmigemo で必要
sudo port -N install nkf

# GNU 版コマンドなど
sudo port -N install findutils
sudo port -N install coreutils

# プログラミング言語
sudo port -N install python36
sudo port -N install go
sudo port -N install opam

# 使うかもしれない
sudo port -N install tree
sudo port -N install wget
sudo port -N install tmux
sudo port -N install tig
sudo port -N install chromedriver
```

### GNU 版コマンドのインストールについて

http://folioscope.hatenablog.jp/entry/2012/09/17/110914


### fish

- `fish_config` でカラーテーマ、prompt を設定する
- `/etc/shells` に fish のパスを追記する

```sh
# ログインシェルを変更
$ chsh -s /opt/local/bin/fish
```

see also

- [fish を導入 - ryskosn log](http://ryskosn.hatenadiary.com/entry/2017/01/02/185151)


### Emacs

- 以前は `Cask` を使っていたが、`use-package` にした
- migemo を別途ビルドする必要あり

`cmigemo` をビルドする

```sh
$ mkdir ~/temp
$ cd ~/temp
$ git clone https://github.com/koron/cmigemo.git
$ cd cmigemo
$ ./configure
$ make osx
$ make osx-dict
$ sudo make osx-install
$ cd ~
$ rm -rf ~/temp
```

see also

- http://rubikitch.com/2014/08/20/migemo/
- http://weblog.ymt2.net/blog/html/2013/08/23/install_migemo_to_emacs_24_3_1.html


### Python

`venv` で `~/py37` に仮想環境を作り、それをメインで使う。

```sh
$ python3.7 -m venv ~/py37
```

```fish
set -x PATH $HOME/py37/bin $PATH
```

`pip` でインストールしたパッケージを一括でアップデートするワンライナー

```sh
$ pip list --outdated | awk '{print $1}' | xargs pip install -U
```

### Go

- `GOPATH` は `$HOME` にする
- `ghq` の root を `$HOME/src` にして一元管理する


### JavaScript

- `nodebrew` で入れる
- [GitHub - hokaccha/nodebrew](https://github.com/hokaccha/nodebrew)


### OCaml

- `opam` で入れる
- [OPAM](https://opam.ocaml.org/)
- [GitHub - ocaml/opam](https://github.com/ocaml/opam)

```sh
$ sudo port install opam
$ opam switch list-available
$ opam switch create 4.07.1
```

### Haskell

- `stack` で入れる
- [The Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/)
- [GitHub - commercialhaskell/stack](https://github.com/commercialhaskell/stack)


### Ruby

- `port select` でシステムの `ruby` から切り替える

```sh
$ port select --list ruby
Available versions for ruby:
	none (active)
	ruby25
```

```sh
$ sudo port select --set ruby ruby25
Password:
Selecting 'ruby25' for 'ruby' succeeded. 'ruby25' is now active.
```

```sh
$ which ruby
/opt/local/bin/ruby

$ which gem
/opt/local/bin/gem
```

#### `sqlint` をインストール

- [GitHub - purcell/sqlint: Simple SQL linter](https://github.com/purcell/sqlint)

```sh
$ sudo gem install sqlint
```


## others

### System settings

#### キーボード

修飾キー `CapsLock` キーを `Control` にする

ショートカット -> キーボード
次のウィンドウを操作対象にする
`Command + F1` -> `Option + Tab` に変更する

ショートカット -> 入力ソース

前の入力ソースを選択 -> チェックを外す
入力メニューの次のソースを選択
`Option + Command + Space` -> `Ctrl + ;`

ショートカット -> Spotlight

Spotlight 検索フィールドを表示
`Ctrl + Space` -> `Option + '`


#### トラックパッド

- ポイントとクリック -> タップでクリックを有効にする
- スクロールとズーム -> スクロールの方向ナチュラルのチェックを外す

#### 共有

コンピュータ名を修正する

#### セキュリティとプライバシー

一般: すべてのアプリケーションを許可


### Finder

- サイドバーからタグを削除
- All my files を非表示
- ホームディレクトリを表示など

### Slate

https://github.com/jigish/slate

```sh
cd /Applications && curl http://www.ninjamonkeysoftware.com/slate/versions/slate-latest.tar.gz | tar -xz
```

```sh
# symlink.sh に含まれている
ln -s ~/dotfiles/.slate.js ~/.slate.js
```

- 初回起動時にアクセシビリティ設定を許可する
  - システム環境設定 -> Security and Privacy -> Privacy -> Accessibility
- メニューバーのアイコンをクリックして `Launch Slate On Login` を選択する

### Gyazo

https://gyazo.com/download

インストールしてログインする
